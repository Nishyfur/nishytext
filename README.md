# NishyText

A simple, efficient text editor with custom syntax highlighting for common languages and lots of uwu

Written in Go and Fyne, modern and portable

## Roadmap

1. Basic editor functionality: Text editing, C syntax highlighting, save/open
2. Theming capability with a simple file system
3. Added languages and bracket pairing (optional: Rainbow brackets mode)
4. "Features": Keysmash generator, uwuified menus
