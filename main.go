package main

import (
	"io"
	"io/fs"
	"io/ioutil"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
)

type texteditor struct {
	app            fyne.App
	mainWindow     fyne.Window
	mainMenu       *fyne.MainMenu
	textEdit       *widget.Entry
	cache          [20]string
	cachePos       int
	openDialog     *dialog.FileDialog
	saveDialog     *dialog.FileDialog
	fileName       string
	filePath       string
	changed        bool
	existingFile   bool
	overrideChange bool
}

func main() {
	NishyText := texteditor{}
	NishyText.init()
	NishyText.mainWindow.ShowAndRun()
}

func (t *texteditor) addToCache(s string) {
	for i := len(t.cache) - 1; i > 0; i-- {
		t.cache[i] = t.cache[i-1]
	}
	t.cache[0] = s
}

func (t *texteditor) resetCache() {
	for i := t.cachePos; i < len(t.cache); i++ {
		t.cache[i-t.cachePos] = t.cache[i]
	}
	t.cachePos = 0
}

func (t *texteditor) zeroCache() {
	t.cache = [20]string{}
}

func (t *texteditor) init() {
	t.app = app.NewWithID("com.gitlab.nishyfur.nishytext")
	t.mainWindow = t.app.NewWindow("NishyText - New Window")
	t.mainWindow.Canvas()
	ctrlZ := &desktop.CustomShortcut{KeyName: fyne.KeyZ, Modifier: fyne.KeyModifierControl}
	ctrlY := &desktop.CustomShortcut{KeyName: fyne.KeyY, Modifier: fyne.KeyModifierControl}
	t.mainWindow.Canvas().AddShortcut(ctrlZ, t.handleWindowShortcuts)
	t.mainWindow.Canvas().AddShortcut(ctrlY, t.handleWindowShortcuts)
	t.openDialog = dialog.NewFileOpen(t.loadFile, t.mainWindow)
	t.saveDialog = dialog.NewFileSave(t.saveFile, t.mainWindow)
	t.textEdit = widget.NewMultiLineEntry()
	t.textEdit.Wrapping = fyne.TextWrapOff
	t.textEdit.TextStyle.Monospace = true
	t.textEdit.OnChanged = t.onTextEditChange
	t.mainWindow.SetContent(container.NewVBox(t.textEdit))

	t.mainMenu = fyne.NewMainMenu()
	t.mainMenu.Items = append(t.mainMenu.Items, fyne.NewMenu("File",
		fyne.NewMenuItem("Open file...", func() { t.openDialog.Show() }),
		fyne.NewMenuItem("Save", func() { t.save() })))
	t.mainMenu.Items = append(t.mainMenu.Items, fyne.NewMenu("Edit",
		fyne.NewMenuItem("Undo", func() { t.undoTextEdit() }),
		fyne.NewMenuItem("Redo", func() { t.redoTextEdit() })))
	t.mainMenu.Items[1].Items[1].Disabled = true
	t.mainWindow.SetMainMenu(t.mainMenu)

	t.mainWindow.Resize(fyne.NewSize(1080, 720))
}

func (t *texteditor) handleWindowShortcuts(shortcut fyne.Shortcut) {
	switch shortcut.ShortcutName() {
	case "CustomDesktop:Control+Z":
		if !t.mainMenu.Items[1].Items[0].Disabled {
			t.undoTextEdit()
		}
	case "CustomDesktop:Control+Y":
		if !t.mainMenu.Items[1].Items[1].Disabled {
			t.redoTextEdit()
		}
	}
}

func (t *texteditor) loadFile(uc fyne.URIReadCloser, err error) {
	defer uc.Close()
	if uc == nil {
		return
	}
	if err != nil {
		panic(err)
	}
	bytes, err := io.ReadAll(uc)
	if err != nil {
		panic(err)
	}
	t.fileName = uc.URI().Name()
	t.filePath = uc.URI().Path()
	t.mainWindow.SetTitle("NishyText - " + t.fileName)
	t.textEdit.SetText(string(bytes))
	t.changed = false
	t.existingFile = true
	t.zeroCache()
	t.addToCache(t.textEdit.Text)
}

func (t *texteditor) save() {
	if t.existingFile {
		err := ioutil.WriteFile(t.filePath, []byte(t.textEdit.Text), fs.ModeAppend)
		if err != nil {
			panic(err)
		}
		t.changed = false
		t.mainWindow.SetTitle("NishyText - " + t.fileName)
	} else {
		t.saveDialog.Show()
	}
}

func (t *texteditor) saveFile(uc fyne.URIWriteCloser, err error) {
	defer uc.Close()
	if uc == nil {
		return
	}
	if err != nil {
		panic(err)
	}
	_, err = uc.Write([]byte(t.textEdit.Text))
	if err != nil {
		panic(err)
	}
	t.fileName = uc.URI().Name()
	t.filePath = uc.URI().Path()
	t.mainWindow.SetTitle("NishyText - " + t.fileName)
	t.changed = false
	t.existingFile = true
	t.zeroCache()
	t.addToCache(t.textEdit.Text)
}

func (t *texteditor) onTextEditChange(s string) {
	if !t.overrideChange {
		if t.cachePos > 0 {
			t.resetCache()
			t.mainMenu.Items[1].Items[0].Disabled = false
			t.mainMenu.Items[1].Items[1].Disabled = true
		}
		t.addToCache(s)
	}
	t.overrideChange = false
	if !t.existingFile {
		return
	}
	if t.changed {
		return
	}
	t.changed = true
	t.mainWindow.SetTitle(t.mainWindow.Title() + " *")
}

func (t *texteditor) undoTextEdit() {
	t.cachePos += 1
	t.overrideChange = true
	t.textEdit.SetText(t.cache[t.cachePos])
	t.mainMenu.Items[1].Items[1].Disabled = false
	if t.cachePos == 19 {
		t.mainMenu.Items[1].Items[0].Disabled = true
	}
}

func (t *texteditor) redoTextEdit() {
	t.cachePos -= 1
	t.overrideChange = true
	t.textEdit.SetText(t.cache[t.cachePos])
	t.mainMenu.Items[1].Items[0].Disabled = false
	if t.cachePos == 0 {
		t.mainMenu.Items[1].Items[1].Disabled = true
	}
}
